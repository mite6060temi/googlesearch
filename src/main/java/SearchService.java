import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Optional;

class SearchService {
    static final String WELCOME_MESSAGE = "Type your search query string and press enter:";
    private static final String GOOGLE_SEARCH_URL = "https://www.google.com/search";

    static String prepareQuery(String searchTerm) {
        String searchQuery = searchTerm.replaceAll(" ", "+");
        return GOOGLE_SEARCH_URL + "?q=" + searchQuery;
    }

        static void printResults(Elements results) {
        if (results.isEmpty()) System.out.println("Google find nothing!");
        else {
            String linkHref = results.first().attr("href");
            String linkText = results.first().text();
            String substring = linkHref.substring(6, linkHref.indexOf("&"));
            System.out.println("Text::" + linkText + ", URL::" + substring);
        }
    }

    static Elements performQuery(String searchURL) {
        Optional<Document> doc = connect(searchURL);
        return getElements(doc);
    }

    static Elements getElements(Optional<Document> doc) {
        return doc.orElse(new Document(""))
                .select("h3.r > a");
    }

    private static Optional<Document> connect(String searchURL) {
        Document doc = null;
        try {
            doc = Jsoup.connect(searchURL).userAgent("Mozilla/5.0").get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(doc);
    }

}
