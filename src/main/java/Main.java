import org.jsoup.select.Elements;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println(SearchService.WELCOME_MESSAGE);
        Scanner scanner = new Scanner(System.in);
        String searchTerm = scanner.nextLine();
        scanner.close();
        String searchURL = SearchService.prepareQuery(searchTerm);
        Elements results = SearchService.performQuery(searchURL);
        SearchService.printResults(results);
    }
}