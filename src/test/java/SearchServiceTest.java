import org.jsoup.Jsoup;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class SearchServiceTest {
    private final String QUERY = "https://www.google.com/search?q=";

    @Test
    public void prepareQuery_handleEmptyString() {
        assertEquals(QUERY, SearchService.prepareQuery(""));
    }

    @Test
    public void prepareQuery_handleStringWithSpace() {
        assertEquals(QUERY + "string+with+spaces+",
                SearchService.prepareQuery("string with spaces "));
    }

    @Test
    public void getElements_returnEmptyElementsIfNoValue() {
        assertEquals(new Elements(), SearchService.getElements(Optional.empty()));
    }

    @Test
    public void getElements_returnEmptyDocIfNull() {
        Document parse =
                Jsoup.parse("<h3 class=\"r\"><a href=\"\">Белорусский портал <b>TUT.BY</b></a></h3>",
                        "");

        String actual = SearchService.getElements(Optional.ofNullable(parse)).text();
        assertEquals("Белорусский портал TUT.BY", actual);
    }


}