import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class SearchServiceHandleElementsTest {
    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void cleanUp() {
        System.setOut(null);
    }

    @Test
    public void printResults_handleEmptyElements() {
        SearchService.printResults(new Elements());
        assertEquals("Google find nothing!", outContent.toString().trim());
    }

    @Test
    public void printResults_handleElements() {
        Document parse =
                Jsoup.parse("<a href=\"/url?q=https://www.tut.by/&amp\">Белорусский портал <b>TUT.BY</b></a>",
                        "https://www.google.by/search?q=tut+by&gws_rd=cr&ei=ydbjWMCPOIa8sQG7wrK4Cg");
        SearchService.printResults(new Elements(parse.select("a")));
        assertEquals("Text::Белорусский портал TUT.BY, URL::=https://www.tut.by/", outContent.toString().trim());
    }
}
