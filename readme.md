## Description

Command-line app that prompts the user for a text string, performs a Google web search
and returns the title and URL of the first result.

### Use:

* Java 8
* Gradle
